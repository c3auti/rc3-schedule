#!/bin/bash

set -e

# assemblies to include
declare -a assemblies=(
    c3auti
)

# single events to include
declare -a events=(
    b824c28f-cd9f-56a6-bfe4-399cbff737a3 # rc3-824289-failed_empathy
    1af62e58-51c1-5e0f-8c01-e5cb3a47d5ee # rc3-162585-autistische_wahrnehmung
)

hq() {
    declare data=$1
    data=${data//&/&amp;}
    data=${data//\"/&quot;}
    data=${data//</&lt;}
    data=${data//>/&gt;}
    printf %s "$data"
}

# We want dates printed in CET
export TZ=UTC-01:00

readobject() {
    declare text=$(jq -Mr 'to_entries | .[] | select(.value != null) | "[\(.key|@sh)]=\(.value|@sh)"')
    eval "$1=($text)"
}

declare accumulator tmp

for a in "${assemblies[@]}"; do
    tmp=$(curl -sf "https://rc3.world/api/c/rc3/assembly/$a/events")
    accumulator+=$(jq -M '.[]' <<<"$tmp")
done

for e in "${events[@]}"; do
    accumulator+=$(curl -sf "https://rc3.world/api/c/rc3/event/$e/")
done

declare -a event_data
eval "event_data=($(jq -sMr 'unique_by(.id) | sort_by(.schedule_start) | .[] | tojson | @sh' <<<"$accumulator"))"

declare -A rooms
declare -A room
fetchroom() {
    [[ -v rooms[$1] ]] || rooms[$1]=$(curl -sf https://rc3.world/api/c/rc3/room/"$1"/)
    readobject room <<<"${rooms[$1]}"
}

declare -A data
hget() { hq "${data[$1]}"; }
declare date
        cat <<EOF
<html>
<head>
<title>c3auti Schedule</title>
<style>
body {
  background-color: #d7b89d;
}
th {
  text-align: left;
}
</style>
</head>
<body>
<h1>c3auti Schedule</h1>
<table>
EOF
for e in "${event_data[@]}"; do
    readobject data <<<"$e"
    curdate=$(date -d "${data[schedule_start]}" +%F)
    if ! [[ "$date" == "$curdate" ]]; then
        date=$curdate
        cat <<EOF
<tr>
<th colspan="3"><h2>$(hq "$date")</h2></th>
</tr>
EOF
    fi
    fetchroom "${data[room]}"
    
    cat <<EOF
<tr>
<td>$(hq "$(date -d "${data[schedule_start]}" +%H:%M)-$(date -d "${data[schedule_end]}" +%H:%M) CET")</td>
<td><a href="https://rc3.world/rc3/room/$(hget room)/">$(hq "${room[name]}")</a></td>
<td><a href="https://rc3.world/rc3/event/$(hget slug)/">$(hget name)</a></td>
</tr>
EOF
done
        cat <<EOF
</table>
</body>
</html>
EOF
